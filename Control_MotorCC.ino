/**
 * Este código controla un motor de corriente continua (CC)
 * haciendo uso de la señal PWM que nos proporciona Arduino
 * 
 * Para ello se emplea como control de potencia un potenciometro,
 * cuya entrada será leida por las entradas analógicas de 
 * Arduino y según estos valores, proporcionar por el pin
 * que lleva la señal PWM, un ciclo de trabajo (CT) mayor o menor.
 */
 
const int pinPotenciometro = A0; // Pin de entrada analógica para el potenciometro
const int pinControlMotor = 9; // Pin para controlar el motor con señal PWM

int valorPotenciometro = 0; // Valor analógico del potenciometro
int valorControlMotor = 0; // Valor para controlar motor con PWM

void setup() {
  // Inicializamos la comunicación serial
  Serial.begin(9600);
}

void loop() {
  valorPotenciometro = analogRead(pinPotenciometro); // Leemos valor potenciometro
  valorControlMotor = map(valorPotenciometro, 0, 1023, 0, 255); // Mapeamos la lectura al rango de salida [0, 255]
  
  analogWrite(pinControlMotor, valorControlMotor); // Aplicamos dicho valor a la salida PWM

  // Mostramos los valores generados
  Serial.print("Potenciometro = ");
  Serial.print(valorPotenciometro);
  Serial.print("\t Salida = ");
  Serial.println(valorControlMotor);

  delay(2); // Pequeño delay por seguridad en lecturas/escrituras
}
