# README - Control de Motor de Corriente Continua (CC) con Señal PWM en Arduino

Este archivo README proporciona una breve introducción y explicación del código que se encuentra a continuación. El código permite controlar un motor de corriente continua (CC) utilizando una señal PWM generada por una placa Arduino. El control de potencia del motor se realiza mediante un potenciómetro, cuya entrada analógica es leída por el Arduino y se utiliza para ajustar el ciclo de trabajo (duty cycle) de la señal PWM proporcionada al motor.

## Descripción del Código

El siguiente código de Arduino controla un motor de corriente continua utilizando una señal PWM generada por el pin 9. El potenciómetro, conectado al pin analógico A0, permite ajustar el valor de control del motor en función de la posición del potenciómetro.

```cpp
const int pinPotenciometro = A0; // Pin de entrada analógica para el potenciómetro
const int pinControlMotor = 9; // Pin para controlar el motor con señal PWM

int valorPotenciometro = 0; // Valor analógico del potenciómetro
int valorControlMotor = 0; // Valor para controlar el motor con PWM
```

- `pinPotenciometro` es el pin de entrada analógica donde se conecta el potenciómetro.
- `pinControlMotor` es el pin que se utiliza para controlar el motor mediante señal PWM.
- `valorPotenciometro` almacena el valor leído desde el potenciómetro.
- `valorControlMotor` representa el valor utilizado para controlar el motor mediante la señal PWM.

```cpp
void setup() {
  // Inicializamos la comunicación serial
  Serial.begin(9600);
}
```

- `setup()` se encarga de la configuración inicial del programa y, en este caso, inicializa la comunicación serial a una velocidad de 9600 baudios para mostrar información en el monitor serial.

```cpp
void loop() {
  valorPotenciometro = analogRead(pinPotenciometro); // Leemos el valor del potenciómetro
  valorControlMotor = map(valorPotenciometro, 0, 1023, 0, 255); // Mapeamos la lectura al rango de salida [0, 255]
  
  analogWrite(pinControlMotor, valorControlMotor); // Aplicamos dicho valor a la salida PWM

  // Mostramos los valores generados
  Serial.print("Potenciómetro = ");
  Serial.print(valorPotenciometro);
  Serial.print("\t Salida = ");
  Serial.println(valorControlMotor);

  delay(2); // Pequeño retraso por seguridad en lecturas/escrituras
}
```

- `loop()` es el bucle principal del programa. En este bucle, se realiza lo siguiente:
  1. Se lee el valor del potenciómetro utilizando `analogRead()`.
  2. Se mapea el valor leído al rango de salida deseado, que es de 0 a 255. Esto se hace con `map()`.
  3. Se aplica el valor mapeado como ciclo de trabajo de la señal PWM al pin de control del motor mediante `analogWrite()`.
  4. Se muestra información sobre el valor del potenciómetro y la salida en el monitor serial para fines de depuración.
  5. Se agrega un pequeño retraso de 2 milisegundos para asegurar una lectura y escritura seguras.

## Uso del Código

Este código permite controlar la velocidad de un motor de corriente continua (CC) mediante un potenciómetro conectado a una entrada analógica de Arduino. El motor se controla ajustando el ciclo de trabajo de la señal PWM proporcionada al motor. Puedes utilizar este código como punto de partida para proyectos que requieran control de motores o dispositivos que respondan a una señal PWM en función de una entrada analógica. Asegúrate de conectar el potenciómetro y el motor según las especificaciones de tu proyecto.